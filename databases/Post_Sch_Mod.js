
const mongoose=require('mongoose');
const PostSch=new mongoose.Schema({
    userId:{
        type:mongoose.Types.ObjectId,
        ref:'user'
    },
    text:{
        type:String
    },
    link:{
        type:String
    }
},
{timestamps:
    true
},
);


const Posts=mongoose.model('posts',PostSch);
module.exports=Posts;