
const mongoose=require('mongoose');
const DetailsSch=new mongoose.Schema({
    fname:{
        type: String,
        required: true
    },
    lname:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true,
        lowercase:true
    },
    password:{
        type: String,
        required: true
    },
    phone:{
        type: String,
        required: true
    },
    jti:{
        type:String,
        required:true
    }
},
{timestamps:
    true
},
    {
        toJSON:{
            transform(doc,ret){
                delete ret.password;
            }
        }
    }
);


const Details=mongoose.model('users',DetailsSch);
module.exports=Details;