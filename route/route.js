// importing files
const express=require('express');
const mongoose=require('mongoose');
require('../cofig');
const Details=require('../databases/Detailes_Sch_Mod');
const jwt=require('jsonwebtoken');
const bcrypt=require('bcrypt');
const multer=require('multer');

const path=require('path');
const upload=require('../multer/multer');
// const uploadposts=require('../multer/multerPosts');
const mongodb=require('mongodb');
const Posts=require('../databases/Post_Sch_Mod');
// destination folder for files
// const upload=multer({dest:'uploads/'})

// create a dummy user= post api
module.exports.dummyuser=async(req,res)=>{
    try{
        let password=req.body.password;
        let hashpwd=await bcrypt.hash(password,10);
        console.log(hashpwd)
        let data=await Details.create({
            fname:req.body.fname,
            lname:req.body.lname,
            email:req.body.email,
            password:hashpwd,
            phone:req.body.password
        })
        return res.status(200).json({
            message:"created successfully!!",
            data:data
        })
    }
    catch(error){
        return res.status(400).json({
            message:"creation unsuccessfull!",
            error:error
        })
    }
}


// for login->generate jwt token
// secret key for jwt
const secretKey="secretkey"
module.exports.login=async(req,res)=>{
    let email=req.body.email;
    let pwd=req.body.password;
    let checkuser=await Details.findOne({email:email});
    // console.log(checkuser);
    if(checkuser){
        let matchpwd=await bcrypt.compare(pwd,checkuser.password);
        console.log(matchpwd);
        let checkId=checkuser._id;
        if(matchpwd){
            // we'll generate the random jti
            let jti=Math.floor(Math.random());
            let jtiUpdate=await Details.updateOne({_id:checkId},{$set:{jti:jti}});
            console.log(jtiUpdate);
            // generating jwt token
            jwt.sign({checkId:checkId, jti:jti},secretKey,{ expiresIn: '1h' },(err,token)=>{
                if(err)
                {
                    console.log(err);
                }
                else{
                    console.log(token);
                    return res.json({token:token});
                }
            })
        }
    }
}


// upload file secure with jwt token
module.exports.uploadfile=async(req,res)=>{
    // console.log()
    res.json(req.file);

}

// validating the jwt token
module.exports.verify=async(req,res,next)=>{
jwt.verify(req.token, secretKey, async (err, authData) => {
    if (err) {
        res.send({ result: "invalid token" })
    } else {
        console.log(authData);
        // verifying the jti whether it exists in the db or not
        
        const jtiId=authData.jti;
        console.log(jtiId);
        // checking if jti exists in db
        let data = await Details.findOne({ _id: authData.checkId },{jti:jtiId});
        console.log(data); 
        if(data)  {
            next();
        }
        else{
            return res.json({
                message:"data doesn't match!!"
            })
        } 
    }
})
}

// verifytoken
module.exports.verifyToken=async(req,res,next)=>{
    const bearerHeader=req.headers['authorization'];
    if(typeof bearerHeader!=='undefined'){
        const bearer=bearerHeader.split(" ");
        const token=bearer[1];
        req.token=token;
        next();
    }
}




// TASK 3
// CRUD FOR POSTS
// create posts
module.exports.createpost=async(req,res)=>{
    try{
        let data=await Posts.create({
            text:req.body.text,
            link:req.body.link,
            userId:req.params
        });
        console.log(data);
        return res.json({
            message:'created successully!!',
            data:data
        })
    }
    catch(error){
        res.status(400).json({
            msg:"can't create posts",
            error:error
        })
    }
}

// update
module.exports.updatepost=async(req,res)=>{
    try{
        let text=req.body.text;
        let link=req.body.link;
        let query={};
        // if(text)
        // {
        //     query.text=text;
        // }
        // else if(link)
        // {
        //     query.link=link;
        // }
        // else
        // {
        //     query.text=text;
        //     query.link=link;
        // }
        // let data=await Posts.updateOne({_id:req.params},{$set:{query:query}});
        let data=await Posts.updateOne({userId:req.params},{$set:{text:req.body.text}})
        console.log(data);
        return res.status(200).json({
            message:'update successful',
            data:data
        })
    }
    catch(error)
    {
        res.status(400).json({
            message:"update unsuccessful",
            error:error
        })
    }
}




// aggregation
// module.exports.aggregation=async(req,res)=>{
//     let userId=req.params;
//     let newid=new mongodb.ObjectId(userId);
//     let data=Details.aggregate(
//         {$match:{userId:newid}},
//         {}
//     )
// }





























// const express=require('express')
// const multer=require('multer')
// const path=require('path')

// let storage=multer.diskStorage({
//     destination:function(req,file,cb){
//         cb(null,'uploads/')
//     },
//     filename:function(req,file,cb){
//         // console.log(file)
//         cb(null,path.extname(file.originalname) )
//     }
// })
// const upload=multer({storage:storage})
// const app=express();

// //upload.single('avatar') is a middleware
// app.post('/api/upload',upload.single('avatar'),(req,res)=>{
//     res.send("upload successfully!!");
// })

// // to access

// app.listen(3000);