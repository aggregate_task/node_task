const express=require('express');
const controller=require('./route');
const router=express.Router();
const upload=require('../multer/multer')
// const uploadposts=require('../multer/multerPosts')
// TASK1
// post api to create a dummy user
router.post('/create',controller.dummyuser);
// generating jwt token
router.post('/login',controller.login);

// TASK2
// upload file secure with jwt token 
router.post('/uploadfile',upload.single('file'),controller.verifyToken,controller.verify,controller.uploadfile);

// CRUD POST
router.post('/createpost/:_id',controller.createpost);
router.put('/updatepost/:userId',controller.updatepost);
module.exports=router;