const express=require('express');
const router=require('./route/user.js');
const morgan=require('morgan');
const app=express();

app.use(express.json());
app.use(morgan("dev"));
app.use('/',router);
app.listen(2000,()=>{
    require('./cofig.js')
})