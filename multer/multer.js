const express=require('express')
const app=express();
const multer=require('multer')
const path=require('path')
// const upload=multer({dest:'uploads/'});
var storage=multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,'uploads/')
    },
    filename:function(req,file,cb){
        console.log(file)
        cb(null,path.extname(file.originalname) )
    }
})
let upload=multer({storage});
module.exports=upload;
// const app=express();

// //upload.single('avatar') is a middleware
// app.post('/api/upload',upload.single('file'),(req,res)=>{
//     // res.json({
//     //     message:"upload successfully!!",
        
//     // });
//     res.json(req.file);

// })

// // to access

// app.listen(1000);