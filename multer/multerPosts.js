const express=require('express')
const app=express();
const multer=require('multer')
const path=require('path')
// const upload=multer({dest:'uploads/'});
var storage=multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,'postsupload/')
    },
    filename:function(req,file,cb){
        console.log(file)
        cb(null,path.extname(file.originalname) )
    }
})
let uploadposts=multer({storage});
module.exports=uploadposts;